﻿CREATE TABLE [feed].[Distance]
(
	[DistanceId] INT IDENTITY (1, 1) NOT NULL, 
	[TerminalCodeFrom] VARCHAR(13) NOT NULL,
	[TerminalCodeTo] VARCHAR(13) NOT NULL,
	[TotalDistance] FLOAT NULL,
	[SECADistance] FLOAT NULL,
	[PiracyDistance] FLOAT NULL
	CONSTRAINT [pkDistance] PRIMARY KEY ([DistanceId])
   
)
