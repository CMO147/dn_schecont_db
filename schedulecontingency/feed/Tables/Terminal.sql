﻿CREATE TABLE [feed].[Terminal]
(
	[TerminalCode] VARCHAR(13) NOT NULL, 
    [PortCode] CHAR(5) NULL, 
    [TerminalName] VARCHAR(50) NULL, 
    [TypeOfLocation] VARCHAR(50) NULL, 
	[HoursPilotIn] FLOAT NULL,
	[HoursPilotOut] FLOAT NULL
	CONSTRAINT [pkTerminal] PRIMARY KEY ([TerminalCode])
)
