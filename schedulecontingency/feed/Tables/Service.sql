﻿CREATE TABLE [feed].[Service]
 (   
    [ServiceCode]          CHAR (3) NOT NULL,
    [TMODE]                CHAR (3) NULL,
    [ServiceName]          CHAR (40) NULL
	CONSTRAINT [pkService] PRIMARY KEY ([ServiceCode])
);