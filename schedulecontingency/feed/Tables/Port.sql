﻿CREATE TABLE [feed].[Port](
	[PortCode]          CHAR (5)      NOT NULL,
    [CountryCode]           CHAR(2)           NULL,
    [PortName]              NVARCHAR (50) NULL,
    [DifferenceFromUTCTime] INT  NULL
	CONSTRAINT [pkPort] PRIMARY KEY ([PortCode])
);