﻿CREATE TABLE [feed].[Vessel] (
    [VesselCode]           CHAR (3)     NOT NULL,
    [VesselName]           VARCHAR (50) NULL,
    [VesselType]           CHAR (3)     NULL,
    [VesselOperatorCode]   CHAR (3)     NULL,
    [VesselOperatorText]   VARCHAR (50) NULL,
	[MaxSpeed]			   FLOAT NULL, 
    [MinSpeed]			   FLOAT NULL
	CONSTRAINT [pkVessel] PRIMARY KEY ([VesselCode])

);

