﻿/*
Deployment script for dnetschedulecontingencydb

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "dnetschedulecontingencydb"
:setvar DefaultFilePrefix "dnetschedulecontingencydb"
:setvar DefaultDataPath ""
:setvar DefaultLogPath ""

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
PRINT N'Altering [dbo].[uspGetSchedulesByVessel]...';


GO

ALTER PROCEDURE [dbo].[uspGetSchedulesByVessel]
	@VesselCode char(3)
AS

	--DECLARE @VesselCode char(3) = '1GS'
	SELECT TOP 20 PortCallId, T.TerminalCode as TerminalCode, T.TerminalName, COALESCE(C.ActualArrivalLocalTime, C.ScheduledArrivalLocalTime, C.ProFormaArrivalLocalTime) AS ArrivalLocalTime, 
		COALESCE(C.ActualDepartureLocalTime, C.ScheduledDepartureLocalTime, C.ProFormaDepartureLocalTime) AS DepartureLocalTime 
		,SiteOrder
	FROM [feed].[Terminal] T	INNER JOIN [feed].[PortCall] C ON T.TerminalCode = C.TerminalCode
		INNER JOIN [feed].[Vessel] V ON V.VesselCode = C.VesselCode
	WHERE SiteOrder >= (SELECT TOP 1 SiteOrder
				FROM  [feed].[PortCall] C 
				WHERE ActualArrivalUTC IS NOT NULL 
					AND VesselCode = @VesselCode
				ORDER BY SiteOrder)
GO
PRINT N'Update complete.';


GO
