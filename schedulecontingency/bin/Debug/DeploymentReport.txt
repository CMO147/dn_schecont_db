﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [feed] (Schema)
       [meta] (Schema)
       [feed].[schecontdb_Distances] (Table)
       [feed].[schecontdb_Port] (Table)
       [feed].[schecontdb_Service] (Table)
       [feed].[schecontdb_PortCall] (Table)
       [feed].[schecontdb_Terminal] (Table)
       [feed].[schecontdb_Vessel] (Table)
       [dbo].[uspGetSchedulesByVessel] (Procedure)

** Supporting actions
