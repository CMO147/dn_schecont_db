﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Drop
       [feed].[fkschecontdb_Distance_TerminalIdFrom] (Foreign Key)
     Create
       [feed].[fkschecontdb_PortCall_PortCallId_Next] (Foreign Key)
       [feed].[fkschecontdb_PortCall_PortCallId_Previous] (Foreign Key)
       [feed].[fkschecontdb_PortCall_PortId_Next] (Foreign Key)
       [feed].[fkschecontdb_PortCall_PortId_Previous] (Foreign Key)
       [feed].[fkschecontdb_PortCall_TerminalId_Next] (Foreign Key)
     Alter
       [dbo].[uspGetSchedulesByVessel] (Procedure)

** Supporting actions

The object [data_0] exists in the target, but it will not be dropped even though you selected the 'Generate drop statements for objects that are in the target database but that are not in the source' check box.
The object [log] exists in the target, but it will not be dropped even though you selected the 'Generate drop statements for objects that are in the target database but that are not in the source' check box.

