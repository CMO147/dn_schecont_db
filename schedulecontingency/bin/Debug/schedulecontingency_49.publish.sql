﻿/*
Deployment script for dnetschedulecontingencydb

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "dnetschedulecontingencydb"
:setvar DefaultFilePrefix "dnetschedulecontingencydb"
:setvar DefaultDataPath ""
:setvar DefaultLogPath ""

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
PRINT N'Starting rebuilding table [feed].[Terminal]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [feed].[tmp_ms_xx_Terminal] (
    [TerminalCode]         VARCHAR (13) NOT NULL,
    [PortCode]             CHAR (5)     NULL,
    [TerminalName]         VARCHAR (50) NULL,
    [TypeOfLocation]       VARCHAR (50) NULL,
    [HoursPilotIn]         FLOAT (53)   NULL,
    [HoursPilotOut]        FLOAT (53)   NULL,
    [Valid_From]           DATETIME     NULL,
    [ID_LogJobLoad_Insert] INT          NULL,
    [ID_LogJobLoad_Update] INT          NULL,
    CONSTRAINT [tmp_ms_xx_constraint_pkTerminal1] PRIMARY KEY CLUSTERED ([TerminalCode] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [feed].[Terminal])
    BEGIN
        INSERT INTO [feed].[tmp_ms_xx_Terminal] ([TerminalCode], [PortCode], [TerminalName], [TypeOfLocation], [Valid_From], [HoursPilotIn], [HoursPilotOut], [ID_LogJobLoad_Insert], [ID_LogJobLoad_Update])
        SELECT   [TerminalCode],
                 [PortCode],
                 [TerminalName],
                 [TypeOfLocation],
                 [Valid_From],
                 [HoursPilotIn],
                 [HoursPilotOut],
                 [ID_LogJobLoad_Insert],
                 [ID_LogJobLoad_Update]
        FROM     [feed].[Terminal]
        ORDER BY [TerminalCode] ASC;
    END

DROP TABLE [feed].[Terminal];

EXECUTE sp_rename N'[feed].[tmp_ms_xx_Terminal]', N'Terminal';

EXECUTE sp_rename N'[feed].[tmp_ms_xx_constraint_pkTerminal1]', N'pkTerminal', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Refreshing [dbo].[uspGetSchedulesByVessel]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[uspGetSchedulesByVessel]';


GO
PRINT N'Update complete.';


GO
