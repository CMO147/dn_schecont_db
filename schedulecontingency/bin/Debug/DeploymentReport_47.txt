﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The type for column CountryCode in table [feed].[schecontdb_Port] is currently  NVARCHAR (8) NULL but is being changed
         to  CHAR (2) NULL. Data loss could occur.

** User actions
     Alter
       [feed].[schecontdb_Port] (Table)

** Supporting actions

The object [data_0] exists in the target, but it will not be dropped even though you selected the 'Generate drop statements for objects that are in the target database but that are not in the source' check box.
The object [log] exists in the target, but it will not be dropped even though you selected the 'Generate drop statements for objects that are in the target database but that are not in the source' check box.
The type for column CountryCode in table [feed].[schecontdb_Port] is currently  NVARCHAR (8) NULL but is being changed to  CHAR (2) NULL. Data loss could occur.

