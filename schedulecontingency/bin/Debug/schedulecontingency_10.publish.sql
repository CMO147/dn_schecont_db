﻿/*
Deployment script for schedulecontingency

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "schedulecontingency"
:setvar DefaultFilePrefix "schedulecontingency"
:setvar DefaultDataPath ""
:setvar DefaultLogPath ""

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
IF EXISTS (SELECT 1
           FROM   [sys].[databases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                NUMERIC_ROUNDABORT OFF,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_CLOSE_ON_COMMIT OFF,
                AUTO_CREATE_STATISTICS ON,
                AUTO_SHRINK OFF,
                AUTO_UPDATE_STATISTICS ON,
                RECURSIVE_TRIGGERS OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [sys].[databases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ALLOW_SNAPSHOT_ISOLATION OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [sys].[databases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_UPDATE_STATISTICS_ASYNC OFF,
                DATE_CORRELATION_OPTIMIZATION OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [sys].[databases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CREATE_STATISTICS ON(INCREMENTAL = OFF) 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [sys].[databases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE (QUERY_CAPTURE_MODE = AUTO, OPERATION_MODE = READ_WRITE, DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_PLANS_PER_QUERY = 200, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), MAX_STORAGE_SIZE_MB = 100) 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [sys].[databases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
    END


GO
IF EXISTS (SELECT 1
           FROM   [sys].[databases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET TEMPORAL_HISTORY_RETENTION ON 
            WITH ROLLBACK IMMEDIATE;
    END


GO
PRINT N'Creating [meta]...';


GO
CREATE SCHEMA [meta]
    AUTHORIZATION [dbo];


GO
PRINT N'Creating [feed]...';


GO
CREATE SCHEMA [feed]
    AUTHORIZATION [dbo];


GO
PRINT N'Creating [feed].[schecontdb_Distances]...';


GO
CREATE TABLE [feed].[schecontdb_Distances] (
    [EK_Distance]     INT  NOT NULL,
    [EK_TerminalFrom] INT  NULL,
    [EK_TerminalTo]   INT  NULL,
    [TotalDistance]   REAL NULL,
    [SECADistance]    REAL NULL,
    [PiracyDistance]  REAL NULL,
    PRIMARY KEY CLUSTERED ([EK_Distance] ASC)
);


GO
PRINT N'Creating [feed].[schecontdb_PortCall]...';


GO
CREATE TABLE [feed].[schecontdb_PortCall] (
    [EK_PortCall]                 INT           NOT NULL,
    [EK_Vessel]                   INT           NULL,
    [EK_Service]                  INT           NULL,
    [EK_Additional]               INT           NULL,
    [ArrivalVoyageCode]           VARCHAR (10)  NULL,
    [DepartureVoyageCode]         VARCHAR (10)  NULL,
    [VoyageDirection]             CHAR (8)      NULL,
    [ScheduledArrivalUTC]         DATETIME2 (7) NULL,
    [ScheduledArrivalLocalTime]   DATETIME2 (7) NULL,
    [ScheduledDepartureUTC]       DATETIME2 (7) NULL,
    [ScheduledDepartureLocalTime] DATETIME2 (7) NULL,
    [ProFormaArrivalUTC]          DATETIME2 (7) NULL,
    [ProFormaArrivalLocalTime]    DATETIME2 (7) NULL,
    [ProFormaDepartureUTC]        DATETIME2 (7) NULL,
    [ProFormaDepartureLocalTime]  DATETIME2 (7) NULL,
    [ActualArrivalUTC]            DATETIME2 (7) NULL,
    [ActualArrivalLocalTime]      DATETIME2 (7) NULL,
    [ActualDepartureUTC]          DATETIME2 (7) NULL,
    [ActualDepartureLocalTime]    DATETIME2 (7) NULL,
    [EK_Terminal_Previous]        INT           NULL,
    [EK_Terminal_Current]         INT           NULL,
    [EK_Terminal_Next]            INT           NULL,
    [Omit]                        CHAR (8)      NULL,
    [OmitReason]                  VARCHAR (50)  NULL,
    [Note]                        VARCHAR (500) NULL,
    [CurrentPort]                 VARCHAR (16)  NULL,
    [NextPort]                    VARCHAR (16)  NULL,
    [PreviousPort]                VARCHAR (16)  NULL,
    [PortCallId]                  INT           NULL,
    [PreviousPortCallId]          INT           NULL,
    [NextPortCallId]              INT           NULL,
    [CurrentTerminal]             VARCHAR (16)  NULL,
    [NextTerminal]                VARCHAR (16)  NULL,
    [PreviousTerminal]            VARCHAR (16)  NULL,
    [Valid_From]                  DATETIME      NULL,
    [ID_LogJobLoad_Insert]        INT           NULL,
    [ID_LogJobLoad_Update]        INT           NULL
);


GO
PRINT N'Creating [feed].[schecontdb_Port]...';


GO
CREATE TABLE [feed].[schecontdb_Port] (
    [EK_Port]               INT           NOT NULL,
    [EK_Country]            INT           NULL,
    [RKSTPortKode]          CHAR (8)      NULL,
    [PortName]              NVARCHAR (50) NULL,
    [DifferenceFromUTCTime] NVARCHAR (8)  NULL,
    [Valid_From]            DATETIME      NULL,
    [ID_LogJobLoad_Insert]  INT           NULL,
    [ID_LogJobLoad_Update]  INT           NULL
);


GO
PRINT N'Creating [feed].[schecontdb_Service]...';


GO
CREATE TABLE [feed].[schecontdb_Service] (
    [EK_Service]           INT      NOT NULL,
    [ServiceCode]          CHAR (3) NULL,
    [TMODE]                CHAR (3) NULL,
    [ServiceName]          CHAR (8) NULL,
    [Valid_From]           DATETIME NULL,
    [ID_LogJobLoad_Insert] INT      NULL,
    [ID_LogJobLoad_Update] INT      NULL
);


GO
PRINT N'Creating [feed].[schecontdb_Vessel]...';


GO
CREATE TABLE [feed].[schecontdb_Vessel] (
    [EK_Vessel]            INT          NOT NULL,
    [VesselCode]           CHAR (3)     NULL,
    [VesselName]           VARCHAR (50) NULL,
    [VesselType]           CHAR (3)     NULL,
    [VesselOperatorCode]   CHAR (3)     NULL,
    [VesselOperatorText]   VARCHAR (50) NULL,
    [MaxSpeed]             REAL         NULL,
    [MinSpeed]             REAL         NULL,
    [Valid_From]           DATETIME     NULL,
    [ID_LogJobLoad_Insert] INT          NULL,
    [ID_LogJobLoad_Update] INT          NULL
);


GO
PRINT N'Creating [feed].[schecontdb_Terminal]...';


GO
CREATE TABLE [feed].[schecontdb_Terminal] (
    [EK_Terminal]          INT          NOT NULL,
    [EK_Port]              INT          NULL,
    [RKSTTerminalCode]     VARCHAR (16) NULL,
    [TerminalName]         VARCHAR (50) NULL,
    [TypeOfLocation]       VARCHAR (50) NULL,
    [Valid_From]           DATETIME     NULL,
    [HoursPilotIn]         REAL         NULL,
    [HoursPilotOut]        REAL         NULL,
    [ID_LogJobLoad_Insert] INT          NULL,
    [ID_LogJobLoad_Update] INT          NULL,
    PRIMARY KEY CLUSTERED ([EK_Terminal] ASC)
);


GO
PRINT N'Creating [dbo].[uspGetSchedulesByVessel]...';


GO
CREATE PROCEDURE [dbo].[uspGetSchedulesByVessel]
	@VesselCode char(3)
AS
	SELECT T.RKSTTerminalCode as TerminalCode, T.TerminalName, COALESCE(C.ActualArrivalLocalTime, C.ScheduledArrivalLocalTime, C.ProFormaArrivalLocalTime) AS ArrivalLocalTime, 
		COALESCE(C.ActualDepartureLocalTime, C.ScheduledDepartureLocalTime, C.ProFormaDepartureLocalTime) AS DepartureLocalTime 

	FROM [feed].[schecontdb_Terminal] T	INNER JOIN [feed].[schecontdb_PortCall] C ON T.EK_Terminal = C.EK_Terminal_Current
		INNER JOIN [feed].[schecontdb_Vessel] V ON V.EK_Vessel = C.EK_Vessel
	WHERE ActualArrivalUTC IS NOT NULL 
		AND V.VesselCode = @VesselCode
	ORDER BY C.ProFormaArrivalUTC
	OFFSET 20 ROWS
GO
DECLARE @VarDecimalSupported AS BIT;

SELECT @VarDecimalSupported = 0;

IF ((ServerProperty(N'EngineEdition') = 3)
    AND (((@@microsoftversion / power(2, 24) = 9)
          AND (@@microsoftversion & 0xffff >= 3024))
         OR ((@@microsoftversion / power(2, 24) = 10)
             AND (@@microsoftversion & 0xffff >= 1600))))
    SELECT @VarDecimalSupported = 1;

IF (@VarDecimalSupported > 0)
    BEGIN
        EXECUTE sp_db_vardecimal_storage_format N'$(DatabaseName)', 'ON';
    END


GO
PRINT N'Update complete.';


GO
