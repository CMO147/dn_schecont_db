﻿** Highlights
     Tables that will be rebuilt
       [feed].[schecontdb_Distances]
       [feed].[schecontdb_Port]
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The column [feed].[schecontdb_Distances].[EK_Distance] is being dropped, data loss could occur.
       The column [feed].[schecontdb_Distances].[EK_TerminalFrom] is being dropped, data loss could occur.
       The column [feed].[schecontdb_Distances].[EK_TerminalTo] is being dropped, data loss could occur.
       The column [feed].[schecontdb_Port].[CountryId] is being dropped, data loss could occur.

** User actions
     Table rebuild
       [feed].[schecontdb_Distances] (Table)
       [feed].[schecontdb_Port] (Table)

** Supporting actions

Your permissions to see all objects in the server or database could not be verified.  The original error was:
The SELECT permission has not been granted on 'sys.sql_logins' for the 'master' database. You must be a member of the 'loginmanager' role to access this system view. 
Reverse Engineer will continue the import process, and logins will not be imported.
The reverse engineering operation will attempt to continue anyway, but the resulting model might be incomplete, malformed, or incorrect.
The column [feed].[schecontdb_Distances].[EK_Distance] is being dropped, data loss could occur.
The column [feed].[schecontdb_Distances].[EK_TerminalFrom] is being dropped, data loss could occur.
The column [feed].[schecontdb_Distances].[EK_TerminalTo] is being dropped, data loss could occur.
The column [feed].[schecontdb_Port].[CountryId] is being dropped, data loss could occur.

