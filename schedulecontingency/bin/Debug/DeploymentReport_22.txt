﻿** Highlights
     Tables that will be rebuilt
       [feed].[schecontdb_Port]
       [feed].[schecontdb_PortCall]
       [feed].[schecontdb_Service]
       [feed].[schecontdb_Terminal]
       [feed].[schecontdb_Vessel]
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       The column [feed].[schecontdb_Port].[EK_Country] is being dropped, data loss could occur.
       The column [feed].[schecontdb_Port].[EK_Port] is being dropped, data loss could occur.
       The column [feed].[schecontdb_PortCall].[EK_Additional] is being dropped, data loss could occur.
       The column [feed].[schecontdb_PortCall].[EK_PortCall] is being dropped, data loss could occur.
       The column [feed].[schecontdb_PortCall].[EK_Service] is being dropped, data loss could occur.
       The column [feed].[schecontdb_PortCall].[EK_Vessel] is being dropped, data loss could occur.
       The column [feed].[schecontdb_Service].[EK_Service] is being dropped, data loss could occur.
       The column [feed].[schecontdb_Terminal].[EK_Port] is being dropped, data loss could occur.
       The column [feed].[schecontdb_Terminal].[EK_Terminal] is being dropped, data loss could occur.
       The column [feed].[schecontdb_Vessel].[EK_Vessel] is being dropped, data loss could occur.

** User actions
     Table rebuild
       [feed].[schecontdb_Port] (Table)
       [feed].[schecontdb_PortCall] (Table)
       [feed].[schecontdb_Service] (Table)
       [feed].[schecontdb_Terminal] (Table)
       [feed].[schecontdb_Vessel] (Table)
     Alter
       [dbo].[uspGetSchedulesByVessel] (Procedure)

** Supporting actions

Your permissions to see all objects in the server or database could not be verified.  The original error was:
The SELECT permission has not been granted on 'sys.sql_logins' for the 'master' database. You must be a member of the 'loginmanager' role to access this system view. 
Reverse Engineer will continue the import process, and logins will not be imported.
The reverse engineering operation will attempt to continue anyway, but the resulting model might be incomplete, malformed, or incorrect.
The column [feed].[schecontdb_Port].[EK_Country] is being dropped, data loss could occur.
The column [feed].[schecontdb_Port].[EK_Port] is being dropped, data loss could occur.
The column [feed].[schecontdb_PortCall].[EK_Additional] is being dropped, data loss could occur.
The column [feed].[schecontdb_PortCall].[EK_PortCall] is being dropped, data loss could occur.
The column [feed].[schecontdb_PortCall].[EK_Service] is being dropped, data loss could occur.
The column [feed].[schecontdb_PortCall].[EK_Vessel] is being dropped, data loss could occur.
The column PortCallId on table [feed].[schecontdb_PortCall] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
The column [feed].[schecontdb_Service].[EK_Service] is being dropped, data loss could occur.
The column [feed].[schecontdb_Terminal].[EK_Port] is being dropped, data loss could occur.
The column [feed].[schecontdb_Terminal].[EK_Terminal] is being dropped, data loss could occur.
The column [feed].[schecontdb_Vessel].[EK_Vessel] is being dropped, data loss could occur.

