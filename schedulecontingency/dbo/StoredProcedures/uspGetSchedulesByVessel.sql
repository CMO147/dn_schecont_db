﻿
CREATE PROCEDURE [dbo].[uspGetSchedulesByVessel]
	@VesselCode char(3)
AS

	--DECLARE @VesselCode char(3) = '1GS'
	SELECT TOP 20 PortCallId, T.TerminalCode as TerminalCode, T.TerminalName, COALESCE(C.ActualArrivalLocalTime, C.ScheduledArrivalLocalTime, C.ProFormaArrivalLocalTime) AS ArrivalLocalTime, 
		COALESCE(C.ActualDepartureLocalTime, C.ScheduledDepartureLocalTime, C.ProFormaDepartureLocalTime) AS DepartureLocalTime 
		,SiteOrder
	FROM [feed].[Terminal] T	INNER JOIN [feed].[PortCall] C ON T.TerminalCode = C.TerminalCode
		INNER JOIN [feed].[Vessel] V ON V.VesselCode = C.VesselCode
	WHERE SiteOrder >= (SELECT TOP 1 SiteOrder
				FROM  [feed].[PortCall] C 
				WHERE ActualArrivalUTC IS NOT NULL 
					AND VesselCode = @VesselCode
				ORDER BY SiteOrder)
	